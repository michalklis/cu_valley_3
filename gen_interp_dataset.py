import numpy as np
import pandas as pd


labels = pd.read_csv("temp_zuz.csv", sep=";")
labels["czas"] = pd.to_datetime(labels["Czas"])
labels = labels.drop("Czas", axis=1).reset_index(drop=True)
labels = pd.merge(
    pd.DataFrame(
        pd.date_range(labels.czas.min(), labels.czas.max(), freq="5min"),
        columns=["czas"],
    ),
    labels,
    how="left",
    on="czas",
)
labels["temp_zuz"] = labels["temp_zuz"].interpolate(
    method="linear", limit_direction="forward", axis=0
)

prev_temp_zuz = np.roll(labels["temp_zuz"], 1)
prev_temp_zuz[0] = prev_temp_zuz[1]
labels["prev_temp_zuz"] = prev_temp_zuz

features = pd.read_feather("features.fth")

to_drop = [
    "woda_chłodząca_do_kolektor_kz9",
    "woda_chłodząca_do_kolektor_kz10",
    "woda_chłodząca_do_kolektor_kz11",
    "woda_chłodząca_do_kolektor_kz12",
    "woda_powrotna_kolektora_kz8",
    "woda_powrotna_kolektora_kz9",
    "woda_powrotna_kolektora_kz10",
    "woda_powrotna_kolektora_kz11",
    "woda_powrotna_kolektora_kz12",
    "woda_powrotna_kolektora_kz13",
    "woda_powrotna_kolektora_kz15",
    "temp._wody_zasil.obieg_pz_1",
    "went_rf02_odcz_zad_obrotów",
    "went_rf03_odcz_zad_obrotów",
    "temp2_pod_2_warstwą_wymurówki",
    "temp3_pod_2_warstwą_wymurówki",
    "temp4_pod_2_warstwą_wymurówki",
    "temp5_pod_2_warstwą_wymurówki",
    "temp6_pod_2_warstwą_wymurówki",
    "temp7_pod_2_warstwą_wymurówki",
    "temp8_pod_2_warstwą_wymurówki",
    "temp9_pod_2_warstwą_wymurówki",
    "temp10_pod_2_warstwą_wymurówki",
    "temp11_pod_2_warstwą_wymurówki",
    "temp12_pod_2_warstwą_wymurówki",
    "temp13_pod_2_warstwą_wymurówki",
    "temp14_pod_2_warstwą_wymurówki",
    "temp16_pod_2_warstwą_wymurówki",
    "temp17_pod_2_warstwą_wymurówki",
    "temp18_pod_2_warstwą_wymurówki",
    "temp19_pod_2_warstwą_wymurówki",
    "temp20_pod_2_warstwą_wymurówki",
    "temp21_pod_2_warstwą_wymurówki",
    "temp22_pod_2_warstwą_wymurówki",
    "temp23_pod_2_warstwą_wymurówki",
    "temp24_pod_2_warstwą_wymurówki",
    "temp_na_kol_kan_1-34",
    "temp_na_kol_kan_35-68",
    "reg_nadawy_koncentratu_liw2",
]
features = features.drop(columns=to_drop)
features = features[features.czas.dt.minute % 5 == 0].reset_index(drop=True)

dataset = pd.merge(features, labels, how="inner", on="czas")
dataset.to_feather("interp_dataset.fth")
