
Żeby odpalić projekt należy:

1. Zainstalować potrzebne paczki:

pip install -r requrements.txt

2. Utworzyć folder data/ do którego należy wrzucić dane do ewaluacji. Ważne aby pliki csv były z prefixem "avg_". 

3. Odpalić skrypt:

bash run_and_eval.sh

Uwaga: Skrypt podaje błąd MSE, nie RMSE.
