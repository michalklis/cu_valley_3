import os
import pickle
import argparse
import numpy as np
import pandas as pd
from tqdm import tqdm

import pandas as pd
from glob import glob


parser = argparse.ArgumentParser()
parser.add_argument("path", help="Path to dataset")
parser.add_argument("model", help="Path to model")
args = parser.parse_args()


def main():
    dataset = prepare_dataset(args.path)
    with open(args.model, "rb") as file:
        model = pickle.load(file)
    result = do_eval(dataset, model)
    print("Model MSE =", result)


def do_eval(dataset, model):
    features = [
        "reg_nadawy_koncentratu_liw1",
        "reg_koncentrat_prazony_liw3",
        "reg_pyl_zwrot_liw4",
        "woda_chłodząca_do_kolektor_kz7",
        "woda_chłodząca_do_kolektor_kz8",
        "woda_chłodząca_do_kolektor_kz13",
        "woda_chłodząca_do_kolektor_kz15",
        "sumaryczna_moc_cieplna_odebrana_całkowita",
        "woda_powrotna_kolektora_kz7",
        "temp1_pod_2_warstwą_wymurówki",
        "temp15_pod_2_warstwą_wymurówki",
        "went_rf01_odcz_zad_obrotów",
        "temp._wody_zasil.obieg_pz_2",
        "prażona_mieszanina_koncentratów_hg1_fe",
        "prażona_mieszanina_koncentratów_hg1_sog",
        "prob_corg",
        "prob_fe",
        "prob_s",
        "prev_temp_zuz",
    ]
    target = "temp_zuz"

    errors = []

    prev_temp_zuz = 1303.7838813542785
    for _, row in tqdm(dataset.iterrows(), total=len(dataset)):
        x, t = row[features[:-1]], row[target]
        x["prev_temp_zuz"] = prev_temp_zuz
        x = x[features].to_numpy()

        t_ = model.predict(x[None, :])
        if not np.isnan(t):
            errors.append((t_.item() - t) ** 2.0)

        prev_temp_zuz = t_ if np.isnan(t) else t

    # print(errors)
    return np.mean(errors)


def prepare_dataset(path):
    dataset_path = f"{path}/avg*"
    print(f'Looking for files in "{dataset_path}"')
    files = glob(dataset_path)
    print(f"Number of data files: {len(files)}")
    df = pd.concat([pd.read_csv(file) for file in files])
    df["czas"] = pd.to_datetime(df["czas"].map(lambda x: x.split("+")[0]))
    df = df.sort_values(by="czas").reset_index(drop=True)

    target = pd.read_csv(os.path.join(path, "temp_zuz.csv"), sep=";")
    target["czas"] = pd.to_datetime(
        target["Czas"].map(lambda x: x.split("+")[0])
    )
    target = target.sort_values(by="czas").reset_index(drop=True)
    target = target.drop(columns=["Czas"])
    names = {
        "001tir01363.daca.pv": "woda_powrotna_kolektora_kz13",
        "001fir01313.daca.pv": "woda_chłodząca_do_kolektor_kz13",
        "001tir01365.daca.pv": "woda_powrotna_kolektora_kz15",
        "001fir01315.daca.pv": "woda_chłodząca_do_kolektor_kz15",
        "001tir01362.daca.pv": "woda_powrotna_kolektora_kz12",
        "001fir01312.daca.pv": "woda_chłodząca_do_kolektor_kz12",
        "001tir01361.daca.pv": "woda_powrotna_kolektora_kz11",
        "001fir01311.daca.pv": "woda_chłodząca_do_kolektor_kz11",
        "001tir01360.daca.pv": "woda_powrotna_kolektora_kz10",
        "001fir01310.daca.pv": "woda_chłodząca_do_kolektor_kz10",
        "001tir01359.daca.pv": "woda_powrotna_kolektora_kz9",
        "001fir01309.daca.pv": "woda_chłodząca_do_kolektor_kz9",
        "001tir01358.daca.pv": "woda_powrotna_kolektora_kz8",
        "001fir01308.daca.pv": "woda_chłodząca_do_kolektor_kz8",
        "001tir01357.daca.pv": "woda_powrotna_kolektora_kz7",
        "001fir01307.daca.pv": "woda_chłodząca_do_kolektor_kz7",
        "037tix00254.daca.pv": "temp._wody_zasil.obieg_pz_1",
        "037tix00264.daca.pv": "temp._wody_zasil.obieg_pz_2",
        "001fcx00211.pv": "reg_nadawy_koncentratu_liw1",
        "001fcx00221.pv": "reg_nadawy_koncentratu_liw2",
        "001fcx00241.pv": "reg_pyl_zwrot_liw4",
        "001fcx00231.pv": "reg_koncentrat_prazony_liw3",
        "001tix01079.daca.pv": "temp17_pod_2_warstwą_wymurówki",
        "001tix01080.daca.pv": "temp18_pod_2_warstwą_wymurówki",
        "001tix01081.daca.pv": "temp19_pod_2_warstwą_wymurówki",
        "001tix01082.daca.pv": "temp20_pod_2_warstwą_wymurówki",
        "001tix01083.daca.pv": "temp21_pod_2_warstwą_wymurówki",
        "001tix01084.daca.pv": "temp22_pod_2_warstwą_wymurówki",
        "001tix01085.daca.pv": "temp23_pod_2_warstwą_wymurówki",
        "001tix01086.daca.pv": "temp24_pod_2_warstwą_wymurówki",
        "001tix01071.daca.pv": "temp9_pod_2_warstwą_wymurówki",
        "001tix01072.daca.pv": "temp10_pod_2_warstwą_wymurówki",
        "001tix01073.daca.pv": "temp11_pod_2_warstwą_wymurówki",
        "001tix01074.daca.pv": "temp12_pod_2_warstwą_wymurówki",
        "001tix01075.daca.pv": "temp13_pod_2_warstwą_wymurówki",
        "001tix01076.daca.pv": "temp14_pod_2_warstwą_wymurówki",
        "001tix01077.daca.pv": "temp15_pod_2_warstwą_wymurówki",
        "001tix01078.daca.pv": "temp16_pod_2_warstwą_wymurówki",
        "001tix01063.daca.pv": "temp1_pod_2_warstwą_wymurówki",
        "001tix01064.daca.pv": "temp2_pod_2_warstwą_wymurówki",
        "001tix01065.daca.pv": "temp3_pod_2_warstwą_wymurówki",
        "001tix01066.daca.pv": "temp4_pod_2_warstwą_wymurówki",
        "001tix01067.daca.pv": "temp5_pod_2_warstwą_wymurówki",
        "001tix01068.daca.pv": "temp6_pod_2_warstwą_wymurówki",
        "001tix01069.daca.pv": "temp7_pod_2_warstwą_wymurówki",
        "001tix01070.daca.pv": "temp8_pod_2_warstwą_wymurówki",
        "prob_corg": "prob_corg",
        "prob_s": "prob_s",
        "prob_fe": "prob_fe",
        "prazonka_fe": "prażona_mieszanina_koncentratów_hg1_fe",
        "prazonka_s": "prażona_mieszanina_koncentratów_hg1_sog",
        "001uxm0rf01.daca.pv": "went_rf01_odcz_zad_obrotów",
        "001uxm0rf02.daca.pv": "went_rf02_odcz_zad_obrotów",
        "001uxm0rf03.daca.pv": "went_rf03_odcz_zad_obrotów",
        "001nir0szr0.daca.pv": "sumaryczna_moc_cieplna_odebrana_całkowita",
        "001txi01153.daca.pv": "temp_na_kol_kan_1-34",
        "001txi01154.daca.pv": "temp_na_kol_kan_35-68",
    }
    names = {k.lower(): v.lower().replace(" ", "_") for k, v in names.items()}
    df = df.rename(names, axis="columns")
    df_merged = pd.merge(df, target, on="czas", how="left")
    to_drop = [
        "woda_chłodząca_do_kolektor_kz9",
        "woda_chłodząca_do_kolektor_kz10",
        "woda_chłodząca_do_kolektor_kz11",
        "woda_chłodząca_do_kolektor_kz12",
        "woda_powrotna_kolektora_kz8",
        "woda_powrotna_kolektora_kz9",
        "woda_powrotna_kolektora_kz10",
        "woda_powrotna_kolektora_kz11",
        "woda_powrotna_kolektora_kz12",
        "woda_powrotna_kolektora_kz13",
        "woda_powrotna_kolektora_kz15",
        "temp._wody_zasil.obieg_pz_1",
        "went_rf02_odcz_zad_obrotów",
        "went_rf03_odcz_zad_obrotów",
        "temp2_pod_2_warstwą_wymurówki",
        "temp3_pod_2_warstwą_wymurówki",
        "temp4_pod_2_warstwą_wymurówki",
        "temp5_pod_2_warstwą_wymurówki",
        "temp6_pod_2_warstwą_wymurówki",
        "temp7_pod_2_warstwą_wymurówki",
        "temp8_pod_2_warstwą_wymurówki",
        "temp9_pod_2_warstwą_wymurówki",
        "temp10_pod_2_warstwą_wymurówki",
        "temp11_pod_2_warstwą_wymurówki",
        "temp12_pod_2_warstwą_wymurówki",
        "temp13_pod_2_warstwą_wymurówki",
        "temp14_pod_2_warstwą_wymurówki",
        "temp16_pod_2_warstwą_wymurówki",
        "temp17_pod_2_warstwą_wymurówki",
        "temp18_pod_2_warstwą_wymurówki",
        "temp19_pod_2_warstwą_wymurówki",
        "temp20_pod_2_warstwą_wymurówki",
        "temp21_pod_2_warstwą_wymurówki",
        "temp22_pod_2_warstwą_wymurówki",
        "temp23_pod_2_warstwą_wymurówki",
        "temp24_pod_2_warstwą_wymurówki",
        "temp_na_kol_kan_1-34",
        "temp_na_kol_kan_35-68",
        "reg_nadawy_koncentratu_liw2",
    ]
    df_merged = df_merged.drop(columns=to_drop)
    df_merged = df_merged[df_merged.czas.dt.minute % 5 == 0]
    # df_merged.fillna(0)
    df_merged.reset_index(inplace=True)
    return df_merged


if __name__ == "__main__":
    main()
