import pickle
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn import tree
from tqdm import tqdm


def do_eval(model, order):
    valid = pd.read_feather("test.fth")
    features = list(set(valid.columns) - {"index", "czas", "temp_zuz"})
    target = "temp_zuz"

    errors = []

    prev_temp_zuz = 1303.7838813542785
    for idx, row in tqdm(valid.iterrows(), total=len(valid)):
        x, t = row[features], row[target]
        x["prev_temp_zuz"] = prev_temp_zuz
        x = x[order].to_numpy().astype(np.float32)

        t_ = model.predict(x[None, :])
        if not np.isnan(t):
            # print(idx, t_, t)
            errors.append((t_.item() - t) ** 2.0)

        prev_temp_zuz = t_ if np.isnan(t) else t
        # prev_temp_zuz = t_

    # print(errors)
    return np.mean(errors)


def main():
    dataset = pd.read_feather("interp_dataset.fth")
    features = [
        "reg_nadawy_koncentratu_liw1",
        "reg_koncentrat_prazony_liw3",
        "reg_pyl_zwrot_liw4",
        "woda_chłodząca_do_kolektor_kz7",
        "woda_chłodząca_do_kolektor_kz8",
        "woda_chłodząca_do_kolektor_kz13",
        "woda_chłodząca_do_kolektor_kz15",
        "sumaryczna_moc_cieplna_odebrana_całkowita",
        "woda_powrotna_kolektora_kz7",
        "temp1_pod_2_warstwą_wymurówki",
        "temp15_pod_2_warstwą_wymurówki",
        "went_rf01_odcz_zad_obrotów",
        "temp._wody_zasil.obieg_pz_2",
        "prażona_mieszanina_koncentratów_hg1_fe",
        "prażona_mieszanina_koncentratów_hg1_sog",
        "prob_corg",
        "prob_fe",
        "prob_s",
        "prev_temp_zuz",
    ]
    target = "temp_zuz"
    x, t = dataset[features].to_numpy(), dataset[target].to_numpy()
    x_train, x_test, t_train, t_test = train_test_split(
        x, t, train_size=0.8, random_state=42
    )

    print("features:", len(features))
    print(features)

    # model = linear_model.LinearRegression()
    # model = linear_model.BayesianRidge()
    # model = GaussianProcessRegressor()
    model = tree.DecisionTreeRegressor(random_state=42)
    model.fit(x_train, t_train)
    with open("decision_tree.pkl", "wb") as file:
        pickle.dump(model, file)
    pred = model.predict(x_test)
    # print(model.coef_)
    print("Easier eval MSE =", np.mean((pred - t_test) ** 2.0))
    print("True eval MSE =", do_eval(model, features))


if __name__ == "__main__":
    main()
