import argparse
import pandas as pd


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("target", type=str)
    parser.add_argument("prediction", type=str)
    parser.add_argument("--target_col", type=str, default="temp_zuz")
    parser.add_argument("--prediction_col", type=str, default="prediction")
    args = parser.parse_args()
    target = pd.read_feather(args.target)
    pred = pd.read_feather(args.prediction)
    assert (target["czas"] == pred["czas"]).all()
    loss = ((target[args.target_col] - pred[args.prediction_col]) ** 2).mean()
    print(f"Output mse: {loss}")


if __name__ == "__main__":
    main()
